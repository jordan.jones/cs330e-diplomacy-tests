from unittest import main, TestCase
from Diplomacy import diplomacy_solve,diplomacy_eval,diplomacy_print
from io import StringIO

class TestDiplomacy(TestCase):
    def test_eval_1(self):
        x = diplomacy_eval(['A Madrid Hold', 'B Barcelona Move Madrid', 'C London Support B'])
        y = ['A [dead]', 'B Madrid', 'C London']
        self.assertEqual(x, y)
    
    def test_eval_2(self):
        x = diplomacy_eval(['A Madrid Hold', 'B Barcelona Move Madrid'])
        y = ['A [dead]', 'B [dead]']
        self.assertEqual(x, y)

    def test_solve_1(self):
        r = StringIO('A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London')
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(),'A [dead]\nB [dead]\nC [dead]\nD [dead]\n')
    
    def test_solve_2(self):
        r = StringIO('A Barcelona Move Madrid\nB London Move Barcelona')
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(),'A Madrid\nB Barcelona\n')
    
    def test_solve_3(self):
        r = StringIO('A Madrid Hold\nB Barcelona Move Madrid\nC London Move Barcelona')
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(),'A [dead]\nB [dead]\nC Barcelona\n')
    

if __name__ == "__main__":
    main()
    
