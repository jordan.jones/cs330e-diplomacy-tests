    #!/usr/bin/env python3

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_solve, diplomacy_read, formatOutput, updateSupportDictionary, diplomacy_eval, diplomacy_generate_results

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):
    def test_Diplomacy_read_1(self):
        r = StringIO("A Berlin Move London\n")
        val = diplomacy_read(r)
        self.assertEqual(val, [['A', 'Berlin', 'Move', 'London']])

    def test_Diplomacy_read_2(self):
        r = StringIO("A Berlin Move London\nB London Hold\nC Rome Support B\n")
        val = diplomacy_read(r)
        self.assertEqual(val, [['A', 'Berlin', 'Move', 'London'], [
                         'B', 'London', 'Hold'], ['C', 'Rome', 'Support', 'B']])

    def test_Diplomacy_format_1(self):
        test = {'A': '[dead]', 'B': 'Moscow', 'C': 'Austin'}
        w = StringIO()
        formatOutput(w, test)
        self.assertEqual(w.getvalue(), "A [dead]\nB Moscow\nC Austin\n")

    def test_Diplomacy_solve_1(self):
        # Everyone Moves Corner Case
        r = StringIO(
            "A Paris Move London\nB London Move Berlin\nC Berlin Move Paris\n")
        w = StringIO()
        val = diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A London\nB Berlin\nC Paris\n")

    def test_Diplomacy_solve_2(self):
        r = StringIO(
            "A Rome Move Berlin\nB London Move Berlin\nC Berlin Hold\nD Moscow Support B\nE Kiev Move Moscow\n")
        w = StringIO()
        val = diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\nE [dead]\n")

    def test_Diplomacy_solve_3(self):
        # Everyone Holds Corner Case
        r = StringIO(
            "A London Hold\nB Berlin Hold\nC Paris Hold\nD Moscow Hold\nE Kiev Hold\n")
        w = StringIO()
        val = diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A London\nB Berlin\nC Paris\nD Moscow\nE Kiev\n")
            
    def test_Diplomacy_solve_4(self):
        # Everyone Moves Corner Case
        r = StringIO("A Madrid Move London\nB London Support Madrid\n")
        w = StringIO()
        val = diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\n")

    def test_Diplomacy_updateSupportDictionary_1(self):
        # removes the support of an army if the supporting army is under attack
        support = {"A": ["B"], "C": ["D"]}
        places = {"Madrid": ["A", "C"], "London": ["B", "D"]}
        expectedUpdatedSupport = {"A": [], "C": []}
        self.assertEqual(expectedUpdatedSupport,
                         updateSupportDictionary(support, places))

    def test_Diplomacy_diplomacy_eval_1(self):
        r = StringIO("A Berlin Move London\nB London Hold\nC Rome Support B\n")
        armyData = diplomacy_read(r)
        expectedArmyStatus = {"A": "[dead]", "B": "London", "C": "Rome"}
        self.assertEqual(expectedArmyStatus, diplomacy_eval(armyData))

    def test_Diplomacy_diplomacy_generate_results_1(self):
        places = {"Madrid": ["A", "C"], "London": ["B", "D"]}
        support = {}
        army_status = {"A": "Madrid", "B": "London",
                       "C": "Madrid", "D": "London"}
        expectedResult = {"A": "[dead]",
                          "B": "[dead]", "C": "[dead]", "D": "[dead]"}
        self.assertEqual(expectedResult, diplomacy_generate_results(
            places, support, army_status))


# ----
# main
# ----
if __name__ == "__main__":
    main()