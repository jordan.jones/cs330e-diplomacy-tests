#!/usr/bin/env python3

# -------
# imports
# -------
from io import StringIO
from unittest import main, TestCase
from Diplomacy import diplomacy_solve, diplomacy_print, diplomacy_read


# -----------
# TestDiplomacy
# -----------
class TestDiplomacy(TestCase):
    # ----
    # solve
    # ----

    # Simple situation for hold
    def test_solve_1(self):
        r = StringIO("A Madrid Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Madrid\n")

    # Simple situation for conflict between two armies
    def test_solve_2(self):
        r = StringIO("A Madrid Hold\n"
                     "B Barcelona Move Madrid\n"
                     "C London Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\n"
                                       "B Madrid\n"
                                       "C London\n")

    # Situation when the supporting army is attacked,
    # and the support nullifies
    def test_solve_3(self):
        r = StringIO("A Madrid Hold\n"
                     "B Barcelona Move Madrid\n"
                     "C London Support B\n"
                     "D Austin Move London\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\n"
                                       "B [dead]\n"
                                       "C [dead]\n"
                                       "D [dead]\n")

    # Situation when support appears before hold and move
    def test_solve_4(self):
        r = StringIO("A Madrid Support B\n"
                     "B London Hold\n"
                     "C Austin Move London\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Madrid\n"
                                       "B London\n"
                                       "C [dead]\n")

    # Situation when there is tie between two armies that have support
    def test_solve_5(self):
        r = StringIO("A Madrid Hold\n"
                     "B Barcelona Move Madrid\n"
                     "C London Move Madrid\n"
                     "D Paris Support B\n"
                     "E Austin Support A\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\n"
                                       "B [dead]\n"
                                       "C [dead]\n"
                                       "D Paris\n"
                                       "E Austin\n")

    # Supporting an attacking army
    def test_solve_6(self):
        r = StringIO("A Madrid Support B\n"
                     "B London Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\n"
                                       "B [dead]\n")

    # Different amount of supports
    def test_solve_7(self):
        r = StringIO("A Madrid Support D\n"
                     "B London Support D\n"
                     "C Austin Support E\n"
                     "D Kiev Move Paris\n"
                     "E Barcelona Move Paris\n"
                     "F Paris Hold\n"
                     "G Dallas Support F\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Madrid\n"
                                       "B London\n"
                                       "C Austin\n"
                                       "D Paris\n"
                                       "E [dead]\n"
                                       "F [dead]\n"
                                       "G Dallas\n")

    # Long list of army interactions
    def test_solve_8(self):
        r = StringIO("A Madrid Hold\n"
                     "B Paris Hold\n"
                     "C Moscow Move Madrid\n"
                     "D Kiev Support C\n"
                     "E Berlin Move Paris\n"
                     "F Austin Support E\n"
                     "G Houston Support F\n"
                     "H Dallas Support F\n"
                     "I Copenhagen Move Austin\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\n"
                                       "B [dead]\n"
                                       "C Madrid\n"
                                       "D Kiev\n"
                                       "E [dead]\n"
                                       "F Austin\n"
                                       "G Houston\n"
                                       "H Dallas\n"
                                       "I [dead]\n")

    # Better coverage
    def test_solve_9(self):
        r = StringIO("A Madrid Move London\n"
                     "B London Support D\n"
                     "C Austin Support D\n"
                     "D Kiev Move Barcelona\n"
                     "E Barcelona Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\n"
                                       "B [dead]\n"
                                       "C Austin\n"
                                       "D Barcelona\n"
                                       "E [dead]\n")

    # ----
    # read
    # ----
    def test_read_1(self):
        s = "A Madrid Hold\n"
        d = diplomacy_read(s)
        self.assertEqual(d, [["A", "Madrid", "Hold"]])

    def test_read_2(self):
        s = "A Madrid Hold\n" \
            "B Barcelona Move Madrid\n" \
            "C London Support B\n"
        d = diplomacy_read(s)
        self.assertEqual(d, [["A", "Madrid", "Hold"], ["B", "Barcelona", "Move", "Madrid"],
                             ["C", "London", "Support", "B"]])

    def test_read_3(self):
        s = "A Madrid Hold\n" \
            "B Barcelona Move Madrid\n" \
            "C London Support B\n" \
            "D Austin Move London\n"
        d = diplomacy_read(s)
        self.assertEqual(d, [["A", "Madrid", "Hold"], ["B", "Barcelona", "Move", "Madrid"],
                             ["C", "London", "Support", "B"], ["D", "Austin", "Move", "London"]])

    # ----
    # print
    # ----
    def test_print_1(self):
        w = StringIO()
        diplomacy_print(w, [["A", "Madrid"]])
        self.assertEqual(w.getvalue(), "A Madrid\n")

    def test_print_2(self):
        w = StringIO()
        diplomacy_print(w, [["A", "[dead]"], ["B", "Madrid"], ["C", "London"]])
        self.assertEqual(w.getvalue(), "A [dead]\n"
                                       "B Madrid\n"
                                       "C London\n")

    def test_print_3(self):
        w = StringIO()
        diplomacy_print(w, [["A", "[dead]"], ["B", "[dead]"]])
        self.assertEqual(w.getvalue(), "A [dead]\n"
                                       "B [dead]\n")


# ----
# main
# ----
if __name__ == "__main__":
    main()

"""
#pragma: no cover
"""