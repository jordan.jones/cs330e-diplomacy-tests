#!/usr/bin/env python3

from io import StringIO
from unittest import main, TestCase

import Diplomacy

class TestDiplomacy(TestCase):

    def test_parse_1(self):
        t = ["A Madrid Hold", "B Barcelona Move Madrid", "C London Support B", "D Austin Move London"]
        v = Diplomacy.diplomacy_parse(t)
        test_location = {'A': 'Madrid', 'B': 'Madrid', 'C': 'London', 'D': 'London'}
        test_power = {'A': 1, 'B': 1, 'C': 1, 'D': 1}
        test_support = {'C': 'B'}
        test_dicts = test_location, test_power, test_support
        self.assertEqual(v, test_dicts)
        Diplomacy.location_dict = {}
        Diplomacy.power_dict= {}
        Diplomacy.support_dict = {}
    
    def test_parse_2(self):
        t = ["A Madrid Hold", "B Barcelona Move Madrid", "C London Support B"]
        v = Diplomacy.diplomacy_parse(t)
        test_location = {'A': 'Madrid', 'B': 'Madrid', 'C': 'London'}
        test_power = {'A': 1, 'B': 1, 'C': 1}
        test_support = {'C': 'B'}
        test_dicts = test_location, test_power, test_support
        self.assertEqual(v, test_dicts)
        Diplomacy.location_dict = {}
        Diplomacy.power_dict= {}
        Diplomacy.support_dict = {}


    def test_support_1(self):
        t = ["A Madrid Hold", "B Barcelona Move Madrid", "C London Support B", "D Austin Move London"]
        r = Diplomacy.diplomacy_parse(t)
        v = Diplomacy.diplomacy_support()
        self.assertEqual(v, {'A': 1, 'B': 1, 'C': 1, 'D': 1})
        Diplomacy.location_dict = {}
        Diplomacy.power_dict= {}
        Diplomacy.support_dict = {}

    def test_support_2(self):
        t = ["A Madrid Hold", "B Barcelona Move Madrid", "C London Support B"]
        r = Diplomacy.diplomacy_parse(t)
        v = Diplomacy.diplomacy_support()
        self.assertEqual(v, {'A': 1, 'B': 2, 'C': 1})
        Diplomacy.location_dict = {}
        Diplomacy.power_dict= {}
        Diplomacy.support_dict = {}
    
    def test_support_3(self):
        t = t = ["A Madrid Hold", "B Barcelona Move Madrid", "C London Move Madrid", "D Paris Support B", "E Austin Support A"]
        r = Diplomacy.diplomacy_parse(t)
        v = Diplomacy.diplomacy_support()
        self.assertEqual(v, {'A': 2, 'B': 2, 'C': 1, 'D': 1, 'E': 1})
        Diplomacy.location_dict = {}
        Diplomacy.power_dict= {}
        Diplomacy.support_dict = {}
    

    def test_eval_1(self):
        t = ["A Madrid Hold"]
        v = Diplomacy.diplomacy_eval(t)
        self.assertEqual(v, ["A Madrid"])
        Diplomacy.location_dict = {}
        Diplomacy.power_dict= {}
        Diplomacy.support_dict = {}

    def test_eval_2(self):
        t = ["A Madrid Hold", "B Barcelona Move Madrid", "C London Support B"]
        v = Diplomacy.diplomacy_eval(t)
        self.assertEqual(v, ["A [dead]", "B Madrid", "C London"])
        Diplomacy.location_dict = {}
        Diplomacy.power_dict= {}
        Diplomacy.support_dict = {}

    def test_eval_3(self):
        t = ["A Madrid Hold", "B Barcelona Move Madrid", "C London Support B", "D Austin Move London"]
        v = Diplomacy.diplomacy_eval(t)
        self.assertEqual(v, ["A [dead]", "B [dead]", "C [dead]", "D [dead]"])
        Diplomacy.location_dict = {}
        Diplomacy.power_dict= {}
        Diplomacy.support_dict = {}

    def test_eval_4(self):
        t = ["A Madrid Hold", "B Barcelona Move Madrid", "C London Move Madrid"]
        v = Diplomacy.diplomacy_eval(t)
        self.assertEqual(v, ["A [dead]", "B [dead]", "C [dead]"])
        Diplomacy.location_dict = {}
        Diplomacy.power_dict= {}
        Diplomacy.support_dict = {}
    
    def test_eval_5(self):
        t = ["A Madrid Hold", "B Barcelona Move Madrid", "C London Move Madrid", "D Paris Support B"]
        v = Diplomacy.diplomacy_eval(t)
        self.assertEqual(v, ["A [dead]", "B Madrid", "C [dead]", "D Paris"])
        Diplomacy.location_dict = {}
        Diplomacy.power_dict= {}
        Diplomacy.support_dict = {}

    def test_eval_6(self):
        t = ["A Madrid Hold", "B Barcelona Move Madrid", "C London Move Madrid", "D Paris Support B", "E Austin Support A"]
        v = Diplomacy.diplomacy_eval(t)
        self.assertEqual(v, ["A [dead]", "B [dead]", "C [dead]", "D Paris", "E Austin"])
        Diplomacy.location_dict = {}
        Diplomacy.power_dict= {}
        Diplomacy.support_dict = {}

    def test_eval_7(self):
        t = ["A Madrid Hold", "B Paris Hold", "C Moscow Move Madrid", "D Kiev Support C", "E Berlin Move Paris", "F Austin Support E", "G Houston Support F", "H Dallas Support F", "I Copenhagen Move Austin"]
        v = Diplomacy.diplomacy_eval(t)
        self.assertEqual(v, ["A [dead]", "B [dead]", "C Madrid", "D Kiev", "E [dead]", "F Austin", "G Houston", "H Dallas", "I [dead]"])
        Diplomacy.location_dict = {}
        Diplomacy.power_dict= {}
        Diplomacy.support_dict = {}

    def test_solve(self):
        r = StringIO("A Madrid Hold\nB Paris Hold\nC Moscow Move Madrid\nD Kiev Support C\nE Berlin Move Paris\nF Austin Support E\nG Houston Support F\nH Dallas Support F\nI Copenhagen Move Austin\n")
        w = StringIO()
        Diplomacy.diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC Madrid\nD Kiev\nE [dead]\nF Austin\nG Houston\nH Dallas\nI [dead]\n")
        Diplomacy.location_dict = {}
        Diplomacy.power_dict= {}
        Diplomacy.support_dict = {}
        




if __name__ == "__main__":
    main()