from io import StringIO
from unittest import main, TestCase
from Diplomacy import diplomacy_solve, diplomacy_read, diplomacy_write


class TestDiplomacy (TestCase):

    # Solve
    def test_solve_1(self):
        inf = "A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n"
        w = StringIO()
        outf = "A [dead]\nB Madrid\nC London\n"
        diplomacy_solve(inf, w)
        self.assertEqual(w.getvalue(), outf)

    def test_solve_2(self):
        inf = "A Madrid Hold\nB Barcelona Move Madrid\n"
        w = StringIO()
        outf = "A [dead]\nB [dead]\n"
        diplomacy_solve(inf, w)
        self.assertEqual(w.getvalue(), outf)

    def test_solve_3(self):
        inf = "A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London\n"
        w = StringIO()
        outf = "A [dead]\nB [dead]\nC [dead]\nD [dead]\n"
        diplomacy_solve(inf, w)
        self.assertEqual(w.getvalue(), outf)

    def test_solve_4(self):
        inf = "A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\n"
        w = StringIO()
        outf = "A [dead]\nB [dead]\nC [dead]\n"
        diplomacy_solve(inf, w)
        self.assertEqual(w.getvalue(), outf)

    # Read

    def test_read_1(self):
        inf = "A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n"
        w = StringIO()
        outf = ['A Madrid Hold', 'B Barcelona Move Madrid', 'C London Support B']
        v = diplomacy_read(inf)
        self.assertEqual(v, outf)

    def test_read_2(self):
        inf = "A Gotham Move Pandora\nB Hyrule Hold\nC Hogwarts Move Azeroth\nD Rapture Hold\nE Winterfell Move Rapture\nF Asgard Move Rapture\nG Azeroth Hold\nH Tatooine Move Hyrule\nI Pandora Hold\n"
        w = StringIO()
        outf = ["A Gotham Move Pandora", "B Hyrule Hold", "C Hogwarts Move Azeroth", "D Rapture Hold",
                "E Winterfell Move Rapture", "F Asgard Move Rapture", "G Azeroth Hold", "H Tatooine Move Hyrule", "I Pandora Hold"]
        v = diplomacy_read(inf)
        self.assertEqual(v, outf)

    def test_read_3(self):
        inf = "A Gotham Move Hyrule\nB Hyrule Move Gotham\nC Hogwarts Support B\nD Rapture Move Reach\nE Winterfell Move Metropolis\nF Asgard Support E\nG Azeroth Move Tatooine\nH Tatooine Hold\nI Pandora Move Tatooine\n"
        w = StringIO()
        outf = ["A Gotham Move Hyrule", "B Hyrule Move Gotham", "C Hogwarts Support B", "D Rapture Move Reach",
                "E Winterfell Move Metropolis", "F Asgard Support E", "G Azeroth Move Tatooine", "H Tatooine Hold", "I Pandora Move Tatooine"]
        v = diplomacy_read(inf)
        self.assertEqual(v, outf)


# Write

def test_write_1(self):
    w = StringIO()
    lst = ["A Gotham Move Pandora", "B Hyrule Hold", "C Hogwarts Move Azeroth", "D Rapture Hold", "E Winterfell Move Rapture",
           "F Asgard Move Rapture", "G Azeroth Hold", "H Tatooine Move Hyrule", "I Pandora Hold"]
    diplomacy_write(lst, w)
    self.assertEqual(
        w.getvalue(), "A Gotham Move Pandora\nB Hyrule Hold\nC Hogwarts Move Azeroth\nD Rapture Hold\nE Winterfell Move Rapture\nF Asgard Move Rapture\nG Azeroth Hold\nH Tatooine Move Hyrule\nI Pandora Hold\n")


def test_write_2(self):
    w = StringIO()
    lst = ['A Madrid Hold', 'B Barcelona Move Madrid', 'C London Support B']
    diplomacy_write(lst, w)
    self.assertEqual(
        w.getvalue(), "A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n")


def test_write_3(self):
    w = StringIO()
    lst = ["A Gotham Move Hyrule", "B Hyrule Move Gotham", "C Hogwarts Support B", "D Rapture Move Reach",
           "E Winterfell Move Metropolis", "F Asgard Support E", "G Azeroth Move Tatooine", "H Tatooine Hold", "I Pandora Move Tatooine"]
    diplomacy_write(lst, w)
    self.assertEqual(
        w.getvalue(), "A Gotham Move Hyrule\nB Hyrule Move Gotham\nC Hogwarts Support B\nD Rapture Move Reach\nE Winterfell Move Metropolis\nF Asgard Support E\nG Azeroth Move Tatooine\nH Tatooine Hold\nI Pandora Move Tatooine\n")


if __name__ == "__main__":  # pragma: no cover
    main()
